//
// Created by jiangxumin on 02/16/22.
//

#include "ars_40x/ros/received_can_msg_ros.hpp"
#include <can_msgs/Frame.h>

namespace ars_40x {
CanMsgsROS::CanMsgsROS(ros::NodeHandle &nh ) 
{
  received_messages_pub_ = nh.advertise<can_msgs::Frame>("received_messages", 10);
}

CanMsgsROS::~CanMsgsROS() {
}

void CanMsgsROS::send_can_msg_topic( uint32_t frame_id, uint8_t dlc, uint8_t data[8]) 
{
  can_msgs::Frame can_msg_;
  can_msg_.id = frame_id;
  can_msg_.is_rtr = false;
  can_msg_.is_extended = false;
  can_msg_.is_error = false;
  can_msg_.dlc = dlc;

  memcpy(&can_msg_.data, data,dlc);

  can_msg_.header.frame_id = "";  // empty frame is the de-facto standard for no frame.
  can_msg_.header.stamp = ros::Time::now();
  received_messages_pub_.publish(can_msg_);
}

}