//
// Created by jiangxumin on 02/16/22.
//

#include "ars_40x/ros/sent_can_msg_ros.hpp"

namespace ars_40x {
SentCanMsgROS::SentCanMsgROS(ros::NodeHandle &nh, ARS_40X_CAN *ars_40x_can) :
    ars_40x_can_(ars_40x_can) {
  send_messages_sub_ = nh.subscribe("sent_messages", 10, &SentCanMsgROS::send_messages_callback, this);

}

SentCanMsgROS::~SentCanMsgROS() {
}

void SentCanMsgROS::send_messages_callback( can_msgs::Frame can_msg ) 
{
     uint8_t array[1024] = {0};
     memcpy(array,&can_msg.data,can_msg.dlc);
    ars_40x_can_->send_can_msg_data( can_msg.id, can_msg.dlc, array);
    // ROS_INFO_STREAM(can_msg);
}

}
