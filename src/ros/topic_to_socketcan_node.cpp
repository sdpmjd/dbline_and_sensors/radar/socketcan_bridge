//
// Created by jiangxumin on 02/16/22.
//

#include "ars_40x/ros/topic_to_socketcan.hpp"

// namespace socketcan_bridge {
namespace ars_40x {
Topic2SocketCan::Topic2SocketCan(ros::NodeHandle &nh,std::string &can_device) :
    nh_(nh),
    sent_can_msg_ros_(nh,this),
    ARS_40X_CAN(can_device)
{

  ros::NodeHandle private_nh("~");
  std::string frame_id;
  // private_nh.param<std::string>("frame_id", frame_id, std::string("radar"));
}

Topic2SocketCan::~Topic2SocketCan() { }

ARS_40X_CAN::ARS_40X_CAN() : can_("can0") { }

ARS_40X_CAN::ARS_40X_CAN(std::string port) : can_(port.c_str()) { }

ARS_40X_CAN::~ARS_40X_CAN() { }

}


int main(int argc, char **argv) 
{
  ros::init(argc, argv, "topic_to_socketcan_node");
  ros::NodeHandle nh;

  std::string can_device = "can0";
  ros::NodeHandle _private_nh("~");
  _private_nh.param<std::string>("can_device", can_device, std::string("can0"));


  ROS_INFO("=========================================");
  ROS_INFO("          topic_to_socketcan_node        ");
  ROS_INFO("=========================================");

  ars_40x::Topic2SocketCan topic_2_socketcan_ros_(nh, can_device);
  ros::spin();
}


