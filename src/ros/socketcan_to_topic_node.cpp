//
// Created by jiangxumin on 02/16/22.
//

#include "ars_40x/ros/socketcan_to_topic.hpp"

// namespace socketcan_bridge {
namespace ars_40x {
SocketCan2Topic::SocketCan2Topic(ros::NodeHandle &nh,std::string &can_device) :
    nh_(nh),
    can_msgs_ros_(nh),
    ARS_40X_CAN(can_device)
{
  ros::NodeHandle private_nh("~");
  std::string frame_id;
}

SocketCan2Topic::~SocketCan2Topic() {
}


ARS_40X_CAN::ARS_40X_CAN() :
    can_("can0") {
}

ARS_40X_CAN::ARS_40X_CAN(std::string port) :
    can_(port.c_str()) {
}

ARS_40X_CAN::~ARS_40X_CAN() {
}


void SocketCan2Topic::send_can_msg_topic( uint32_t frame_id, uint8_t dlc, uint8_t data[8])
{
  can_msgs_ros_.send_can_msg_topic( frame_id, dlc, data);
};

void SocketCan2Topic::receive_data() 
{
  while (ros::ok()) {
    receive_can_data();
  }
}

void SocketCan2Topic::run() {
  receive_data_thread_ = std::thread(std::bind(&SocketCan2Topic::receive_data, this));
  receive_data_thread_.detach();
}
}

int main(int argc, char **argv) {
  ros::init(argc, argv, "socketcan_to_topic_node");
  ros::NodeHandle nh;

  std::string can_device = "can0";
  ros::NodeHandle _private_nh("~");
  _private_nh.param<std::string>("can_device", can_device, std::string("can0"));

  ROS_INFO("=========================================");
  ROS_INFO("         socketcan_to_topic_node         ");
  ROS_INFO("=========================================");
  ars_40x::SocketCan2Topic socketcan_2_topic_ros_(nh, can_device);
  socketcan_2_topic_ros_.run();
  ros::spin();
}

