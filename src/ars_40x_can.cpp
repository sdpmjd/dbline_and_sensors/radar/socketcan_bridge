//
// Created by jiangxumin on 01/16/22.
//

#include "ars_40x/ars_40x_can.hpp"

namespace ars_40x {
ARS_40X_CAN::ARS_40X_CAN() :
    can_("can0") {
}

ARS_40X_CAN::ARS_40X_CAN(std::string port) :
    can_(port.c_str()) {
}

ARS_40X_CAN::~ARS_40X_CAN() {
}

bool ARS_40X_CAN::receive_can_data() {
  uint32_t frame_id;
  uint8_t dlc;
  uint8_t data[8] = {0};
  bool read_status = can_.read(&frame_id, &dlc, data);
  if (!read_status) {
    return false;
  }

  send_can_msg_topic(frame_id, dlc, data);

  return true;
}

bool ARS_40X_CAN::send_can_msg_data(uint32_t frame_id, uint8_t dlc, uint8_t data[8]) 
{
      can_.write(frame_id, dlc, data);
  return true;
}

}
