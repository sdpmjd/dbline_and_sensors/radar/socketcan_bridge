//
// Created by jiangxumin on 9/13/19.
//

#ifndef ARS_40X_ARS_40X_HPP
#define ARS_40X_ARS_40X_HPP

#include <socket_can/socket_can.hpp>

#include <cstdint>
#include <string>
#include <iostream>

namespace ars_40x {

class ARS_40X_CAN {
 public:
  ARS_40X_CAN();

  ARS_40X_CAN(std::string port);

  ~ARS_40X_CAN();

  virtual bool receive_can_data();

  virtual bool send_can_msg_data(uint32_t frame_id, uint8_t dlc, uint8_t data[8]);
  virtual void send_can_msg_topic( uint32_t frame_id, uint8_t dlc, uint8_t data[8]){};

 private:
  socket_can::SocketCAN can_;

};
}

#endif //ARS_40X_ARS_40X_HPP
