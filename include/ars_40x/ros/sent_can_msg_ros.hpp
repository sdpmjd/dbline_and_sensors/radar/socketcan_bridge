//
// Created by jiangxumin on 02/16/22.
//

#ifndef ARS_40X_RADAR_CFG_ROS_HPP
#define ARS_40X_RADAR_CFG_ROS_HPP

#include <ros/ros.h>

#include <cstdint>

#include "ars_40x/ars_40x_can.hpp"
#include "can_msgs/Frame.h"

namespace ars_40x {
class SentCanMsgROS {
 public:
  SentCanMsgROS(ros::NodeHandle &nh, ARS_40X_CAN *ars_40x_can);

  ~SentCanMsgROS();

 void send_messages_callback( can_msgs::Frame can_msg ); 

 private:
  ARS_40X_CAN *ars_40x_can_;
  ros::Subscriber send_messages_sub_;

};
}

#endif //ARS_40X_RADAR_CFG_ROS_HPP
