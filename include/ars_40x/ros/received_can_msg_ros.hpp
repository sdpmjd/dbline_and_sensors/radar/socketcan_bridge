//
// Created by jiangxumin on 02/16/22.
//

#ifndef ARS_40X_OBJECT_LIST_ROS_HPP
#define ARS_40X_OBJECT_LIST_ROS_HPP

#include <ros/ros.h>

#include <cstdint>
#include <can_msgs/Frame.h>
#include "ars_40x/ars_40x_can.hpp"

namespace ars_40x {
class CanMsgsROS {
 public:
  CanMsgsROS(ros::NodeHandle &nh);
  ~CanMsgsROS();
  void send_can_msg_topic( uint32_t frame_id, uint8_t dlc, uint8_t data[8]);

 private:
  ros::Publisher received_messages_pub_;
  can_msgs::Frame can_msg_;
};
}

#endif //ARS_40X_OBJECT_LIST_ROS_HPP
