//
// Created by jiangxumin on 02/16/22.
//

#ifndef ARS_40X_SocketCanBridgeRos_HPP
#define ARS_40X_SocketCanBridgeRos_HPP

#include <ros/ros.h>

#include "ars_40x/ars_40x_can.hpp"

#include "ars_40x/ros/sent_can_msg_ros.hpp"

namespace ars_40x  {

class Topic2SocketCan : public ARS_40X_CAN {
 public:

  Topic2SocketCan(ros::NodeHandle &nh, std::string &can_device );
  ~Topic2SocketCan();

 private:
  ros::NodeHandle nh_;

  SentCanMsgROS sent_can_msg_ros_;

};
}

#endif //ARS_40X_SocketCanBridgeRos_HPP
