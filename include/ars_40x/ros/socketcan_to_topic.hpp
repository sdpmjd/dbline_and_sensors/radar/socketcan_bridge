//
// Created by jiangxumin on 02/16/22.
//

#ifndef ARS_40X_SocketCanBridgeRos_HPP
#define ARS_40X_SocketCanBridgeRos_HPP

#include <ros/ros.h>
#include <thread>

#include "ars_40x/ars_40x_can.hpp"

#include "ars_40x/ros/received_can_msg_ros.hpp"

namespace ars_40x  {

class SocketCan2Topic : public ARS_40X_CAN {
 public:

  SocketCan2Topic(ros::NodeHandle &nh, std::string &can_device );
  ~SocketCan2Topic();
  void receive_data();
  void run();

  void send_can_msg_topic( uint32_t frame_id, uint8_t dlc, uint8_t data[8]);

 private:
  ros::NodeHandle nh_;
  std::thread receive_data_thread_;

  CanMsgsROS can_msgs_ros_;
};
}

#endif //ARS_40X_SocketCanBridgeRos_HPP
